package main

import (
	"fmt"
	"net"
	"time"
)

func main() {
	startTime := time.Now()
	fmt.Printf("op_latency_ms{op=set} %f\n", float64(time.Since(startTime).Nanoseconds())/1e6)

	// connect tcp
	conn, err := net.Dial("tcp", postgresConfig.host+":"+postgresConfig.port)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("op_latency_ms{op=get} %f\n", float64(time.Since(startTime).Nanoseconds())/1e6)

	// close connection
	conn.Close()
}
