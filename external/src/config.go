package main

import (
	"os"
)

type PostgresConfig struct {
	host string
	port string
}

var postgresConfig = PostgresConfig{
	host: getEnv("POSTGRES_HOST", "192.168.64.47"),
	port: getEnv("POSTGRES_PORT", "5432"),
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}
