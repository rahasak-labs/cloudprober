# cloudprober

## external prober

custom prober which implemented with golang

```
# cross compile
env GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o build/tcpprober -ldflags "extldflags=-static" src/*.go
```

```
# copy to minikube
scp -r -i $(minikube ssh-key) ~/Workspace/rahasak/labs/cloudprober/tcp/build/tcpprober docker@$(minikube ip):/private/var/services/prober
```

```
# run with docker
docker run -v /private/var/services/prober/tcpprober.cfg:/etc/cloudprober.cfg -v /private/var/services/prober/tcpprober:/tcpprober cloudprober/cloudprober
```



## k8s prober

running cloudprober on kubernets, used tcp, http and rds probes

```
# deploy endpoints
kubectl apply -f db1-endpoint.yaml
kubectl apply -f db2-endpoint.yaml
```

```
# deploy rbac to access k8s resources
kubectl apply -f rds/rds-cloudprober-rbac.yaml
```

```
# deploy cloudprober which monitoring kubernetes endpoints
kubectl apply -f rds/rds-cloudprober-deployment.yaml
```


## http prober

http prober to monitor http endpoint



## tcp prober

tcp prober to monitor tcp endpoint



## docker

dockerized cloudprober
